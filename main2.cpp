#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "./tntjama/tnt.h"
#include "mcorr.h"
#include "mmult.h"
#include "mport.h"
#include "./prob/prob.cpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::setw;

int main(){
	int n=10000,p=10;
	TNT::Array2D<long double> mat(n,p);
	double Alpha;
	std::ifstream fin;
	fin.open("vma1_2.txt");
	if(!fin.is_open()){
		std::cout<<"Error opening input file!"<<std::endl;
		exit(1);
	}
	
	for(int i=0;i<n;i++)
		for(int j=0;j<p;j++)
			fin>>mat[i][j];
			
	fin.close();
	
	Alpha=0.01;
	
	mcorr mcorr1=mcorr(n,p,mat,Alpha);
	
	mcorr1.mcorr_pairCorr(0);
	
	mcorr1.mcorr_pairCorr(1);
	
	mcorr1.mcorr_pairCorr(2);
	
	mmult mmult1=mmult(n,p,mat,Alpha);
	mmult1.mmult_LRT();
	
	mport mport1=mport(n,p,mat,Alpha);
	mport1.mport_centerMat();
	mport1.mport_portmanteauTests(p);
	mport1.mport_mahdiMcLeod(p);
	return 0;
}
