#include <math.h>
#include <iostream>
#include <fstream>

/**
 * 1   M = 0.861s
 * 10  M = 8.245s
 * 50  M = 38.629s
 * 100 M = 76.151s
 * 
 * 
 **/


using namespace std;
typedef unsigned long long int64;

int64 mult(int64, int64);
void reccurence (int64, int, int64);
void genStatus(int, int64 *);
void genSequence(int64, int64);
void CreerMat(int64, int);

int main() {
	
	/* x_(n+1) = [ x_n * (x_n + 1) ] mod 2^64 */
	/* Statut initial :  x_0 = 1 */
	
	
	
	int64 init = 1;
	int nb_occ = pow(2,20);
	int64 occ = pow(2,20);
	
	int64 n=pow(2,13);
	int p=4;
	
	
	CreerMat(n,p);
	
	//reccurence(init, nb_occ, occ);

	return 0;
}

int64 mult (int64 a, int64 b) {
	return (a*b)%((int64)pow(2, 64));
}


void reccurence (int64 init, int nb_occ, int64 occ) {
	ofstream myfile;
	myfile.open ("gen_alea.txt");
	myfile << "Nombre n° 1 valeur: " << init << endl;
	for(int j=0; j<nb_occ; j++)
	{
		for(int64 i=0; i<occ;i++)
		{
			init = mult(init, init+1);
		}
		myfile << "Nombre n° " << occ * (j+1) << " valeur: " << init << endl;
	}
	myfile.close();
}


void CreerMat(int64 n, int p) 
{
	int64 status[p];
	genStatus(p, status);
	
	for(int a=0; a<p; a++)
	{
		genSequence(n, status[a]);
	} 
	ifstream files[p];
	for(int k=0; k<p ; k++)
	{
		string name = "Sequence_";
		name += to_string(status[k]);
		files[k].open(name);
	}
	
	ofstream myfile;
	myfile.open("vma1_2.txt", std::ofstream::out | std::ofstream::trunc);
	
	for(int i=0; i<n; i++)
	{
		string line;
		for(int j=0;j<p;j++)
		{
			files[j] >> line;
			myfile << line << " ";
		}
		myfile << endl;
	}
		
	myfile.close();
	
	for(int j=0; j<p ; j++)
	{
		files[j].close();
	}	
}


void genSequence(int64 n, int64 status)
{
	string name = "Sequence_";
	name += to_string(status);
	
	int64 val=status;
	
	ofstream myfile;
	myfile.open(name, std::ofstream::out | std::ofstream::trunc);
	
	myfile << val << endl;
	
	for(int64 i=0; i<n;i++)
	{
		val = mult(val, val+1);
		myfile << val << endl;
	}
	
	myfile.close();
	
}

void genStatus(int p, int64 * status) {
	string name = "gen_alea.txt";
	
	string line;
	ifstream is(name);
	
	string tmp;
	
	for(int j=0; j<p; j++)
	{
		is >> tmp >> tmp >> tmp >> tmp >> status[j];
		
		for(int64 i=0; i<pow(2,18); i++)
		{
			getline(is, line);
		}
	}
}
